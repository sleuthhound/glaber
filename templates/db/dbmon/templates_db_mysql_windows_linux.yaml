zabbix_export:
  version: '5.4'
  date: '2022-01-31T17:59:54Z'
  groups:
    -
      uuid: 41751c98af5442e58e28d80905552276
      name: DBMON.Templates/Databases
  templates:
    -
      uuid: 190690805fc1479da828bec73dc5311c
      template: 'DBMON_Template MySQL for Linux'
      name: 'MySQL for Linux (Active, DBMON)'
      description: |
        Template for monitoring MySQL DB for Linux (active agent).
        
        This template was developed by Mikhail Grigorev.
        
        Support e-mail: sleuthhound@gmail.com
        
        Version: 1.0.0
      templates:
        -
          name: 'DBMON_Template DB MySQL'
      groups:
        -
          name: DBMON.Templates/Databases
      items:
        -
          uuid: 424b46d4dd8647f3ac7d24fe857b867a
          name: '[MySQL] Service ''{$DBMON_MYSQL_SERVICE_NAME}'', instance ''{$DBMON_MYSQL_INSTANCE}'': state'
          type: ZABBIX_ACTIVE
          key: 'proc.num[{$DBMON_MYSQL_SERVICE_NAME},{$DBMON_MYSQL_SERVICE_USER},,{$DBMON_MYSQL_SERVICE_CMD}]'
          history: 30d
          tags:
            -
              tag: 'MySQL Info'
          triggers:
            -
              uuid: cd9dfc652e8f42138a8fd3d735c994b8
              expression: 'max(/DBMON_Template MySQL for Linux/proc.num[{$DBMON_MYSQL_SERVICE_NAME},{$DBMON_MYSQL_SERVICE_USER},,{$DBMON_MYSQL_SERVICE_CMD}],2m)=0'
              recovery_mode: RECOVERY_EXPRESSION
              recovery_expression: 'min(/DBMON_Template MySQL for Linux/proc.num[{$DBMON_MYSQL_SERVICE_NAME},{$DBMON_MYSQL_SERVICE_USER},,{$DBMON_MYSQL_SERVICE_CMD}],3m)>0'
              name: 'Service ''{$DBMON_MYSQL_SERVICE_NAME}'' is not running'
              priority: DISASTER
              description: |
                Основной процесс '{$DBMON_MYSQL_SERVICE_NAME}' экземпляра '{$DBMON_MYSQL_INSTANCE}' был остановлен или аварийно завершил работу (crash).
                Зайдите на сервер и проверьте состояние экземпляра, проверьте журнал error.log на предмет ошибок работы.
                Доведите информацию до заказчика и эксперта.
              tags:
                -
                  tag: Instance
                  value: '{$DBMON_MYSQL_INSTANCE}'
      tags:
        -
          tag: OS
          value: Linux
      macros:
        -
          macro: '{$DBMON_MYSQL_SERVICE_CMD}'
        -
          macro: '{$DBMON_MYSQL_SERVICE_NAME}'
          value: mysqld
        -
          macro: '{$DBMON_MYSQL_SERVICE_USER}'
          value: mysql
    -
      uuid: ebb4217bc0174e9fade1e89b1d31ee20
      template: 'DBMON_Template MySQL for Windows'
      name: 'MySQL for Windows (Active, DBMON)'
      description: |
        Template for monitoring MySQL DB for Windows (active agent).
        
        This template was developed by Mikhail Grigorev.
        
        Support e-mail: sleuthhound@gmail.com
        
        Version: 1.0.0
      templates:
        -
          name: 'DBMON_Template DB MySQL'
      groups:
        -
          name: DBMON.Templates/Databases
      items:
        -
          uuid: 29f5ba0600a24b248f1bb4306529c5de
          name: '[MySQL] Service ''{$DBMON_MYSQL_SERVICE_EXE_NAME}'', instance ''{$DBMON_MYSQL_INSTANCE}'': CPU usage (% Processor Time)'
          type: ZABBIX_ACTIVE
          key: 'perf_counter_en["\Process({$DBMON_MYSQL_SERVICE_EXE_NAME})\% Processor Time"]'
          history: 30d
          value_type: FLOAT
          description: 'Processor Time shows the amount of CPU usage that the process is using, note that a multi-threaded process on a multi-core server could drive this counter above 100%.'
          preprocessing:
            -
              type: DISCARD_UNCHANGED_HEARTBEAT
              parameters:
                - 10m
          tags:
            -
              tag: 'MySQL Info'
        -
          uuid: 3748a30be5774fad9d3970893ef8d299
          name: '[MySQL] Service ''{$DBMON_MYSQL_SERVICE_EXE_NAME}'', instance ''{$DBMON_MYSQL_INSTANCE}'': Memory usage (Working Set)'
          type: ZABBIX_ACTIVE
          key: 'perf_counter_en["\Process({$DBMON_MYSQL_SERVICE_EXE_NAME})\Working Set"]'
          history: 30d
          units: B
          description: 'Working Set shows the amount of physical memory the process is using.'
          preprocessing:
            -
              type: DISCARD_UNCHANGED_HEARTBEAT
              parameters:
                - 10m
          tags:
            -
              tag: 'MySQL Info'
        -
          uuid: 8d8acba64e334e29aad704106568be80
          name: '[MySQL] Service ''{$DBMON_MYSQL_SERVICE_NAME}'', instance ''{$DBMON_MYSQL_INSTANCE}'': startup type'
          type: ZABBIX_ACTIVE
          key: 'service.info[{$DBMON_MYSQL_SERVICE_NAME},startup]'
          history: 30d
          valuemap:
            name: 'Windows service startup type'
          tags:
            -
              tag: 'MySQL Info'
        -
          uuid: 2097c86197ae4c57a1edfc380b7bd7e0
          name: '[MySQL] Service ''{$DBMON_MYSQL_SERVICE_NAME}'', instance ''{$DBMON_MYSQL_INSTANCE}'': state'
          type: ZABBIX_ACTIVE
          key: 'service.info[{$DBMON_MYSQL_SERVICE_NAME},state]'
          history: 30d
          valuemap:
            name: 'Windows service state'
          tags:
            -
              tag: 'MySQL Info'
        -
          uuid: e8caf88428d5431f922b4ea37a6d45f1
          name: '[MySQL] Service ''{$DBMON_MYSQL_SERVICE_NAME}'', instance ''{$DBMON_MYSQL_INSTANCE}'': account'
          type: ZABBIX_ACTIVE
          key: 'service.info[{$DBMON_MYSQL_SERVICE_NAME},user]'
          delay: 1h
          history: 30d
          trends: '0'
          value_type: CHAR
          valuemap:
            name: 'Windows service startup type'
          tags:
            -
              tag: 'MySQL Info'
      tags:
        -
          tag: OS
          value: Windows
      macros:
        -
          macro: '{$DBMON_MYSQL_SERVICE_EXE_NAME}'
          value: mysqld
        -
          macro: '{$DBMON_MYSQL_SERVICE_NAME}'
          value: MySQL
      valuemaps:
        -
          uuid: f3901b5b2c494a76a4cd8406f200afba
          name: 'Windows service startup type'
          mappings:
            -
              value: '0'
              newvalue: Automatic
            -
              value: '1'
              newvalue: 'Automatic delayed'
            -
              value: '2'
              newvalue: Manual
            -
              value: '3'
              newvalue: Disabled
            -
              value: '4'
              newvalue: Unknown
        -
          uuid: 670711550df2448399d87865c635c3db
          name: 'Windows service state'
          mappings:
            -
              value: '0'
              newvalue: Running
            -
              value: '1'
              newvalue: Paused
            -
              value: '2'
              newvalue: 'Start pending'
            -
              value: '3'
              newvalue: 'Pause pending'
            -
              value: '4'
              newvalue: 'Continue pending'
            -
              value: '5'
              newvalue: 'Stop pending'
            -
              value: '6'
              newvalue: Stopped
            -
              value: '7'
              newvalue: Unknown
            -
              value: '255'
              newvalue: 'No such service'
  triggers:
    -
      uuid: 6d3adc3327634ccb86e0cbe82351815a
      expression: 'last(/DBMON_Template MySQL for Windows/service.info[{$DBMON_MYSQL_SERVICE_NAME},state])=255'
      recovery_mode: RECOVERY_EXPRESSION
      recovery_expression: 'max(/DBMON_Template MySQL for Windows/service.info[{$DBMON_MYSQL_SERVICE_NAME},state],2m)=0 or last(/DBMON_Template MySQL for Windows/service.info[{$DBMON_MYSQL_SERVICE_NAME},startup])=3'
      name: 'Service ''{$DBMON_MYSQL_SERVICE_NAME}'' is not found'
      priority: HIGH
      description: |
        Служба '{$DBMON_MYSQL_SERVICE_NAME}' не найдена.
        Создайте или исправьте макрос DBMON_MYSQL_SERVICE_NAME у хоста, содержащий корректное имя службы MySQL.
        В противном случае мониторинг будет работать некорректно.
      tags:
        -
          tag: Service
          value: '{$DBMON_MYSQL_SERVICE_NAME}'
    -
      uuid: f1bda6f988e245d9b638d20603be5b09
      expression: 'last(/DBMON_Template MySQL for Windows/service.info[{$DBMON_MYSQL_SERVICE_NAME},state])<>0 and last(/DBMON_Template MySQL for Windows/service.info[{$DBMON_MYSQL_SERVICE_NAME},startup])<>3'
      recovery_mode: RECOVERY_EXPRESSION
      recovery_expression: 'max(/DBMON_Template MySQL for Windows/service.info[{$DBMON_MYSQL_SERVICE_NAME},state],3m)=0 or last(/DBMON_Template MySQL for Windows/service.info[{$DBMON_MYSQL_SERVICE_NAME},startup])=3'
      name: 'Service ''{$DBMON_MYSQL_SERVICE_NAME}'' is not running'
      opdata: 'current state: {ITEM.LASTVALUE1}, startup type: {ITEM.LASTVALUE2}'
      priority: DISASTER
      description: |
        Служба '{$DBMON_MYSQL_SERVICE_NAME}' была остановлена или аварийно завершила работу (crash).
        Зайдите на сервер и проверьте состояние службы, проверьте журнал событий Windows на предмет ошибок работы службы.
        Доведите информацию до заказчика и эксперта.
      dependencies:
        -
          name: 'Service ''{$DBMON_MYSQL_SERVICE_NAME}'' is not found'
          expression: 'last(/DBMON_Template MySQL for Windows/service.info[{$DBMON_MYSQL_SERVICE_NAME},state])=255'
          recovery_expression: 'max(/DBMON_Template MySQL for Windows/service.info[{$DBMON_MYSQL_SERVICE_NAME},state],2m)=0 or last(/DBMON_Template MySQL for Windows/service.info[{$DBMON_MYSQL_SERVICE_NAME},startup])=3'
      tags:
        -
          tag: Service
          value: '{$DBMON_MYSQL_SERVICE_NAME}'
