Name:		glaber
Version:	2.0.7
Release:	%{?alphatag:0.}1%{?alphatag}%{?dist}
Summary:	The Enterprise-class open source monitoring solution
Group:		Applications/Internet
License:	GPLv2+
URL:		http://glaber.io
Source0:	%{name}-%{version}%{?alphatag}.tar.gz
Source1:	zabbix-logrotate.in
Source2:	zabbix-tmpfiles.conf
Source3:	zabbix-agent-dbmon.service
Source4:	zabbix-agent-dbmon@.service
Source5:	zabbix-agent-dbmon.init
Source6:	zabbix-agent-dbmon.sysconfig

Buildroot:	%{_tmppath}/zabbix-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  make
# FIXME: Building debuginfo is broken on RHEL 5 & 8. Disabled for now.
%if 0%{?rhel} <= 5 || 0%{?rhel} >= 8
%define debug_package %{nil}
%endif

%if 0%{?rhel} >= 8
BuildRequires:  mariadb-connector-c-devel
BuildRequires:  postgresql-devel >= 12.0
%endif
%if 0%{?rhel} == 7
BuildRequires:	mysql-devel >= 5.5
BuildRequires:	postgresql-devel >= 9.2
%endif
%if 0%{?rhel} == 6
BuildRequires:  mysql-devel >= 5.1
BuildRequires:  postgresql-devel >= 8.4
%endif
BuildRequires:	curl-devel >= 7.13.1
BuildRequires:	libxml2-devel
BuildRequires:	pcre-devel
BuildRequires:	libevent-devel
%if 0%{?rhel} >= 6
BuildRequires:	openssl-devel >= 1.0.1
%endif
%if 0%{?rhel} >= 7
BuildRequires:	systemd
%endif
BuildRequires:  libconfig-devel

%description
Glaber is a fork of Zabbix with efficiency and speed improvements.
Zabbix is the ultimate enterprise-level software designed for
real-time monitoring of millions of metrics collected from tens of
thousands of servers, virtual machines and network devices.

%package agent-dbmon
Summary:       Glaber Agent for Database monitoring
Group:         Applications/Internet
Requires:      logrotate
Requires(pre):      /usr/sbin/useradd
%if 0%{?rhel} >= 7
Requires(post):     systemd
Requires(preun):    systemd
Requires(preun):    systemd
%else
Requires(post):     /sbin/chkconfig
Requires(preun):    /sbin/chkconfig
Requires(preun):    /sbin/service
Requires(postun):   /sbin/service
%endif
Obsoletes:          zabbix

%description agent-dbmon
Glaber agent for Database monitoring.

%package get
Summary:                Zabbix Get
Group:                  Applications/Internet

%description get
Zabbix get command line utility.

%package sender
Summary:                Zabbix Sender
Group:                  Applications/Internet

%description sender
Zabbix sender command line utility.

%prep
%setup0 -q -n %{name}-%{version}%{?alphatag}

%build

build_dbmon_flags="
        --enable-dependency-tracking
        --sysconfdir=/etc/zabbix
        --libdir=%{_libdir}/zabbix
        --enable-agent
        --enable-dbmon
        --enable-dbmon-mysql
        --enable-dbmon-postgresql
        --enable-ipv6
        --with-libcurl
        --with-libpcre
        --with-libpthread
        --with-mysql
        --with-postgresql
"

%if 0%{?rhel} >= 6
build_dbmon_flags="$build_dbmon_flags --with-openssl"
%endif

%configure $build_dbmon_flags
make %{?_smp_mflags}
cp src/zabbix_agent/zabbix_agentd src/zabbix_agent/zabbix_agentd_dbmon

%install

rm -rf $RPM_BUILD_ROOT

# install
make DESTDIR=$RPM_BUILD_ROOT install

# install necessary directories
mkdir -p $RPM_BUILD_ROOT%{_localstatedir}/log/zabbix
mkdir -p $RPM_BUILD_ROOT%{_localstatedir}/run/zabbix
mkdir -p $RPM_BUILD_ROOT%{_localstatedir}/lib/zabbix

# install zabbix_agentd_dbmon
install -m 0755 -p src/zabbix_agent/zabbix_agentd_dbmon $RPM_BUILD_ROOT%{_sbindir}/
#rm -f $RPM_BUILD_ROOT%{_sbindir}/zabbix_agentd

# install configuration files
mkdir $RPM_BUILD_ROOT%{_sysconfdir}/zabbix/zabbix_agentd_dbmon.d
install -m 0644 conf/zabbix_agentd_dbmon/userparameter_dbmon.conf $RPM_BUILD_ROOT%{_sysconfdir}/zabbix/zabbix_agentd_dbmon.d
install -Dm 0755 conf/zabbix_agentd_dbmon/dbmon.sh $RPM_BUILD_ROOT%{_sysconfdir}/zabbix/zabbix_agentd_dbmon.d
install -m 0644 conf/zabbix_agentd_dbmon.sql.conf $RPM_BUILD_ROOT%{_sysconfdir}/zabbix

cat conf/zabbix_agentd_dbmon.conf | sed \
        -e '/^# PidFile=/a \\nPidFile=%{_localstatedir}/run/zabbix/zabbix_agentd_dbmon.pid' \
        -e 's|^LogFile=.*|LogFile=%{_localstatedir}/log/zabbix/zabbix_agentd_dbmon.log|g' \
        -e '/^# LogFileSize=.*/a \\nLogFileSize=5' \
        -e '/^# Include=$/a \\nInclude=%{_sysconfdir}/zabbix/zabbix_agentd_dbmon.d/*.conf' \
        > $RPM_BUILD_ROOT%{_sysconfdir}/zabbix/zabbix_agentd_dbmon.conf
cp man/zabbix_agentd_dbmon.man $RPM_BUILD_ROOT%{_mandir}/man8/zabbix_agentd_dbmon.8

# install logrotate configuration files
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/logrotate.d
cat %{SOURCE1} | sed \
        -e 's|COMPONENT|agentd-dbmon|g' \
        > $RPM_BUILD_ROOT%{_sysconfdir}/logrotate.d/zabbix-agent-dbmon

# install startup scripts
%if 0%{?rhel} >= 7
install -Dm 0644 -p %{SOURCE3} $RPM_BUILD_ROOT%{_unitdir}/zabbix-agent-dbmon.service
install -Dm 0644 -p %{SOURCE4} $RPM_BUILD_ROOT%{_unitdir}/zabbix-agent-dbmon@.service
%else
install -Dm 0755 -p %{SOURCE5} $RPM_BUILD_ROOT%{_sysconfdir}/init.d/zabbix-agent-dbmon
%endif

%if 0%{?rhel} <= 6
install -Dm 0644 -p %{SOURCE6} $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/zabbix-agent-dbmon
%endif

# install systemd-tmpfiles conf
%if 0%{?rhel} >= 7
install -Dm 0644 -p %{SOURCE2} $RPM_BUILD_ROOT%{_prefix}/lib/tmpfiles.d/zabbix-agent-dbmon.conf
%endif

%clean
rm -rf $RPM_BUILD_ROOT

%pre agent-dbmon
getent group zabbix > /dev/null || groupadd -r zabbix
getent passwd zabbix > /dev/null || \
	useradd -r -g zabbix -d %{_localstatedir}/lib/zabbix -s /sbin/nologin \
	-c "Zabbix Monitoring System" zabbix
mkdir -p %{_localstatedir}/lib/zabbix > /dev/null
chown -R zabbix:zabbix %{_localstatedir}/lib/zabbix > /dev/null
:

%post agent-dbmon
%if 0%{?rhel} >= 7
%systemd_post zabbix-agent-dbmon.service
%else
/sbin/chkconfig --add zabbix-agent-dbmon || :
%endif
:

%posttrans agent-dbmon
# preserve old userparameter_dbmon.conf file during upgrade
if [ -f %{_sysconfdir}/zabbix/zabbix_agentd_dbmon.d/userparameter_dbmon.conf.rpmsave ] && [ ! -f %{_sysconfdir}/zabbix/zabbix_agentd_dbmon.d/userparameter_dbmon.conf ]; then
        cp -vn %{_sysconfdir}/zabbix/zabbix_agentd_dbmon.d/userparameter_dbmon.conf.rpmsave %{_sysconfdir}/zabbix/zabbix_agentd_dbmon.d/userparameter_dbmon.conf
fi
# move zabbix_agentd_dbmon_sql.conf to zabbix_agentd_dbmon.sql.conf
if [ -f %{_sysconfdir}/zabbix/zabbix_agentd_dbmon_sql.conf ]; then
        yes | mv %{_sysconfdir}/zabbix/zabbix_agentd_dbmon_sql.conf %{_sysconfdir}/zabbix/zabbix_agentd_dbmon.sql.conf
fi
:

%preun agent-dbmon
if [ "$1" = 0 ]; then
%if 0%{?rhel} >= 7
%systemd_preun zabbix-agent-dbmon.service
%else
/sbin/service zabbix-agent-dbmon stop >/dev/null 2>&1
/sbin/chkconfig --del zabbix-agent-dbmon
%endif
fi
:

%postun agent-dbmon
%if 0%{?rhel} >= 7
%systemd_postun_with_restart zabbix-agent-dbmon.service
%else
if [ $1 -ge 1 ]; then
/sbin/service zabbix-agent-dbmon try-restart >/dev/null 2>&1 || :
fi
%endif

%files agent-dbmon
%exclude %{_sysconfdir}/zabbix/zabbix_agentd.conf
%exclude %{_sbindir}/zabbix_agentd
%exclude %{_mandir}/man8/zabbix_agentd.8.gz
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING NEWS README
%config(noreplace) %{_sysconfdir}/zabbix/zabbix_agentd_dbmon.conf
%config(noreplace) %{_sysconfdir}/logrotate.d/zabbix-agent-dbmon
%dir %{_sysconfdir}/zabbix/zabbix_agentd_dbmon.d
%config(noreplace) %{_sysconfdir}/zabbix/zabbix_agentd_dbmon.d/userparameter_dbmon.conf
%config %{_sysconfdir}/zabbix/zabbix_agentd_dbmon.d/dbmon.sh
%config(noreplace) %{_sysconfdir}/zabbix/zabbix_agentd_dbmon.sql.conf
%attr(0755,zabbix,zabbix) %dir %{_localstatedir}/log/zabbix
%attr(0755,zabbix,zabbix) %dir %{_localstatedir}/run/zabbix
%attr(0755,zabbix,zabbix) %dir %{_localstatedir}/lib/zabbix
%{_sbindir}/zabbix_agentd_dbmon
%{_mandir}/man8/zabbix_agentd_dbmon.8*
%if 0%{?rhel} >= 7
%{_unitdir}/zabbix-agent-dbmon.service
%{_unitdir}/zabbix-agent-dbmon@.service
%{_prefix}/lib/tmpfiles.d/zabbix-agent-dbmon.conf
%else
%{_sysconfdir}/init.d/zabbix-agent-dbmon
%endif

%files get
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING NEWS README
%{_bindir}/zabbix_get
%{_mandir}/man1/zabbix_get.1*

%files sender
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING NEWS README
%{_bindir}/zabbix_sender
%{_mandir}/man1/zabbix_sender.1*

%changelog
* Thu Jan 07 2021 Zabbix Packager <info@zabbix.com> - 5.2.4-1
- update to 5.2.4
- reworked spec file to allow selecting which packages are being built via macros (ZBX-18826)

* Mon Dec 21 2020 Zabbix Packager <info@zabbix.com> - 5.2.3-1
- update to 5.2.3

* Mon Nov 30 2020 Zabbix Packager <info@zabbix.com> - 5.2.2-1
- update to 5.2.2
- added proxy and java-gateway to rhel-7

* Fri Oct 30 2020 Zabbix Packager <info@zabbix.com> - 5.2.1-1
- update to 5.2.1

* Mon Oct 26 2020 Zabbix Packager <info@zabbix.com> - 5.2.0-1
- update to 5.2.0

* Thu Oct 22 2020 Zabbix Packager <info@zabbix.com> - 5.2.0-0.7rc2
- update to 5.2.0rc2

* Tue Oct 20 2020 Zabbix Packager <info@zabbix.com> - 5.2.0-0.6rc1
- update to 5.2.0rc1

* Mon Oct 12 2020 Zabbix Packager <info@zabbix.com> - 5.2.0-0.5beta2
- update to 5.2.0beta2

* Mon Sep 28 2020 Zabbix Packager <info@zabbix.com> - 5.2.0-0.4beta1
- update to 5.2.0beta1
- added User=zabbix & Group=zabbix to all service files

* Mon Sep 14 2020 Zabbix Packager <info@zabbix.com> - 5.2.0-0.3alpha3
- update to 5.2.0alpha3
- added separate zabbix-web-deps package
- doing hardened builds on rhel >= 8
- removed libyaml.patch
- overriding ExternalScripts & AlertScriptsPath in binaries instead of config files (ZBX-17983)

* Mon Aug 31 2020 Zabbix Packager <info@zabbix.com> - 5.2.0-0.2alpha2
- update to 5.2.0alpha2
- building only agent, sender & get packages on rhel <= 7
- creating empty log file for agent2 (ZBX-18243)

* Mon Aug 17 2020 Zabbix Packager <info@zabbix.com> - 5.2.0-0.1alpha1
- update to 5.2.0alpha1
- building server and proxy with mysql 8 & postgresql 12 on rhel/centos 7 (ZBX-18221)
- added various After=postgresql* directives to server & proxy service files (ZBX-17492)

* Mon Jul 13 2020 Zabbix Packager <info@zabbix.com> - 5.0.2-1
- update to 5.0.2
- removed ZBX-17801 patch
- added "if build_agent2" around zabbix_agent2.conf installation (ZBX-17818)

* Thu May 28 2020 Zabbix Packager <info@zabbix.com> - 5.0.1-1
- update to 5.0.1
- changed mysql build dependency on rhel/centos-8 from mysql-devel to mariadb-connector-c-devel (ZBX-17738)
- added patch that fixes (ZBX-17801)

* Mon May 11 2020 Zabbix Packager <info@zabbix.com> - 5.0.0-1
- update to 5.0.0

* Tue May 05 2020 Zabbix Packager <info@zabbix.com> - 5.0.0-0.7rc1
- update to 5.0.0rc1
- moved frontends/php to ui directory

* Mon Apr 27 2020 Zabbix Packager <info@zabbix.com> - 5.0.0-0.6beta2
- update to 5.0.0beta2

* Tue Apr 14 2020 Zabbix Packager <info@zabbix.com> - 5.0.0-0.5beta1
- update to 5.0.0beta1
- added agent2 on rhel/centos 7

* Mon Mar 30 2020 Zabbix Packager <info@zabbix.com> - 5.0.0-0.4alpha4
- update to 5.0.0alpha4
- removed proxy, java-gateway & js packages on rhel 5 & 6 due to minimum supported database version increase

* Mon Mar 16 2020 Zabbix Packager <info@zabbix.com> - 5.0.0-0.3alpha3
- update to 5.0.0alpha3
- using libssh instead of libssh2 (rhel/centos 8)
- removed explicit dependency on php from zabbix-web (rhel/centos 8)
- removed explicit dependency on httpd from zabbix-web (rhel/centos 7)
- added zabbix-apache-conf (rhel/centos 7)
- using zabbix-web-database-scl as zabbix-(apache/nginx)-conf package dependency (rhel/centos 7)

* Mon Feb 17 2020 Zabbix Packager <info@zabbix.com> - 5.0.0-0.2alpha2
- update to 5.0.0alpha2
- fixed font configuration in pre/post scriptlets on rhel-8

* Wed Feb 05 2020 Zabbix Packager <info@zabbix.com> - 5.0.0-0.2alpha1
- added *-scl packages to help with resolving php7.2+ and nginx dependencies of zabbix frontend on rhel/centos 7
- added posttrans script that preserves /etc/zabbix/zabbix_agentd.d/userparameter_mysql.conf file
- added config(noreplace) to /etc/sysconfig/zabbix-agent
- added explicit version to php-module dependencies in zabbix-web package on rhel/centos 8

* Mon Jan 27 2020 Zabbix Packager <info@zabbix.com> - 5.0.0-0.1alpha1
- update to 5.0.0alpha1

* Tue Jan 07 2020 Zabbix Packager <info@zabbix.com> - 4.4.4-2
- build of rhel-5 packages to be resigned with gpg version 3

* Thu Dec 19 2019 Zabbix Packager <info@zabbix.com> - 4.4.4-1
- update to 4.4.4
- added After=<database>.service directives to server and proxy service files

* Wed Nov 27 2019 Zabbix Packager <info@zabbix.com> - 4.4.3-1
- update to 4.4.3
- added User=zabbix and Group=zabbix directives to agent service file

* Mon Nov 25 2019 Zabbix Packager <info@zabbix.com> - 4.4.2-1
- update to 4.4.2

* Mon Oct 28 2019 Zabbix Packager <info@zabbix.com> - 4.4.1-1
- update to 4.4.1

* Mon Oct 07 2019 Zabbix Packager <info@zabbix.com> 4.4.0-1
- update to 4.4.0

* Thu Oct 03 2019 Zabbix Packager <info@zabbix.com> - 4.4.0-0.5rc1
- update to 4.4.0rc1

* Tue Sep 24 2019 Zabbix Packager <info@zabbix.com> - 4.4.0-0.4beta1
- update to 4.4.0beta1
- added zabbix-agent2 package

* Wed Sep 18 2019 Zabbix Packager <info@zabbix.com> - 4.4.0-0.3alpha3
- update to 4.4.0alpha3

* Thu Aug 15 2019 Zabbix Packager <info@zabbix.com> - 4.4.0-0.2alpha2
- update to 4.4.0alpha2
- using google-noto-sans-cjk-ttc-fonts for graphfont in web-japanese package on rhel-8
- added php-fpm as dependency of zabbix-web packages on rhel-8

* Wed Jul 17 2019 Zabbix Packager <info@zabbix.com> - 4.4.0-0.1alpha1
- update to 4.4.0alpha1
- removed apache config from zabbix-web package
- added dedicated zabbix-apache-conf and zabbix-nginx-conf packages

* Fri Mar 29 2019 Zabbix Packager <info@zabbix.com> - 4.2.0-1
- update to 4.2.0
- removed jabber notifications support and dependency on iksemel library

* Tue Mar 26 2019 Zabbix Packager <info@zabbix.com> - 4.2.0-0.6rc2
- update to 4.2.0rc2

* Mon Mar 18 2019 Zabbix Packager <info@zabbix.com> - 4.2.0-0.5rc1
- update to 4.2.0rc1

* Mon Mar 04 2019 Zabbix Packager <info@zabbix.com> - 4.2.0-0.4beta2
- update to 4.2.0beta2

* Mon Feb 18 2019 Zabbix Packager <info@zabbix.com> - 4.2.0-0.1beta1
- update to 4.2.0beta1

* Tue Feb 05 2019 Zabbix Packager <info@zabbix.com> - 4.2.0-0.3alpha3
- build of 4.2.0alpha3 with *.mo files

* Wed Jan 30 2019 Zabbix Packager <info@zabbix.com> - 4.2.0-0.2alpha3
- added timescaledb.sql.gz to zabbix-server-pgsql package

* Mon Jan 28 2019 Zabbix Packager <info@zabbix.com> - 4.2.0-0.1alpha3
- update to 4.2.0alpha3

* Fri Dec 21 2018 Zabbix Packager <info@zabbix.com> - 4.2.0-0.2alpha2
- update to 4.2.0alpha2

* Tue Nov 27 2018 Zabbix Packager <info@zabbix.com> - 4.2.0-0.1alpha1
- update to 4.2.0alpha1

* Mon Oct 29 2018 Zabbix Packager <info@zabbix.com> - 4.0.1-1
- update to 4.0.1

* Mon Oct 01 2018 Zabbix Packager <info@zabbix.com> - 4.0.0-2
- update to 4.0.0

* Fri Sep 28 2018 Zabbix Packager <info@zabbix.com> - 4.0.0-1.1rc3
- update to 4.0.0rc3

* Tue Sep 25 2018 Zabbix Packager <info@zabbix.com> - 4.0.0-1.1rc2
- update to 4.0.0rc2

* Wed Sep 19 2018 Zabbix Packager <info@zabbix.com> - 4.0.0-1.1rc1
- update to 4.0.0rc1

* Mon Sep 10 2018 Zabbix Packager <info@zabbix.com> - 4.0.0-1.1beta2
- update to 4.0.0beta2

* Tue Aug 28 2018 Zabbix Packager <info@zabbix.com> - 4.0.0-1.1beta1
- update to 4.0.0beta1

* Mon Jul 23 2018 Zabbix Packager <info@zabbix.com> - 4.0.0-1.1alpha9
- update to 4.0.0alpha9
- add PHP variable max_input_vars = 10000, overriding default 1000

* Mon Jun 18 2018 Zabbix Packager <info@zabbix.com> - 4.0.0-1.1alpha8
- update to 4.0.0alpha8

* Wed May 30 2018 Zabbix Packager <info@zabbix.com> - 4.0.0-1.1alpha7
- update to 4.0.0alpha7

* Fri Apr 27 2018 Zabbix Packager <info@zabbix.com> - 4.0.0-1.1alpha6
- update to 4.0.0alpha6
- add support for Ubuntu 18.04 (Bionic)
- move enabling JMX interface on Zabbix java gateway to zabbix_java_gateway.conf

* Mon Mar 26 2018 Vladimir Levijev <vladimir.levijev@zabbix.com> - 4.0.0-1.1alpha5
- update to 4.0.0alpha5

* Tue Feb 27 2018 Vladimir Levijev <vladimir.levijev@zabbix.com> - 4.0.0-1.1alpha4
- update to 4.0.0alpha4

* Mon Feb 05 2018 Vladimir Levijev <vladimir.levijev@zabbix.com> - 4.0.0-1.1alpha3
- update to 4.0.0alpha3

* Tue Jan 09 2018 Vladimir Levijev <vladimir.levijev@zabbix.com> - 4.0.0-1.1alpha2
- update to 4.0.0alpha2

* Tue Dec 19 2017 Vladimir Levijev <vladimir.levijev@zabbix.com> - 4.0.0-1alpha1
- update to 4.0.0alpha1

* Thu Nov 09 2017 Vladimir Levijev <vladimir.levijev@zabbix.com> - 3.4.4-2
- add missing translation (.mo) files

* Tue Nov 07 2017 Vladimir Levijev <vladimir.levijev@zabbix.com> - 3.4.4-1
- update to 3.4.4
- fix issue with new line character in pid file that resulted in failure when shutting down daemons on RHEL 5

* Tue Oct 17 2017 Vladimir Levijev <vladimir.levijev@zabbix.com> - 3.4.3-1
- update to 3.4.3

* Mon Sep 25 2017 Vladimir Levijev <vladimir.levijev@zabbix.com> - 3.4.2-1
- update to 3.4.2

* Mon Aug 28 2017 Vladimir Levijev <vladimir.levijev@zabbix.com> - 3.4.1-1
- update to 3.4.1
- change SocketDir to /var/run/zabbix

* Mon Aug 21 2017 Vladimir Levijev <vladimir.levijev@zabbix.com> - 3.4.0-1
- update to 3.4.0

* Wed Apr 26 2017 Kodai Terashima <kodai.terashima@zabbix.com> - 3.4.0-1alpha1
- update to 3.4.0alpla1 r68116
- add libpcre and libevent for compile option

* Sun Apr 23 2017 Kodai Terashima <kodai.terashima@zabbix.com> - 3.2.5-1
- update to 3.2.5
- add TimeoutSec=0 to systemd service file

* Thu Mar 02 2017 Kodai Terashima <kodai.terashima@zabbix.com> - 3.2.4-2
- remove TimeoutSec for systemd

* Mon Feb 27 2017 Kodai Terashima <kodai.terashima@zabbix.com> - 3.2.4-1
- update to 3.2.4
- add TimeoutSec for systemd service file

* Wed Dec 21 2016 Kodai Terashima <kodai.terashima@zabbix.com> - 3.2.3-1
- update to 3.2.3

* Thu Dec 08 2016 Kodai Terashima <kodai.terashima@zabbix.com> - 3.2.2-1
- update to 3.2.2

* Sun Oct 02 2016 Kodai Terashima <kodai.terashima@zabbix.com> - 3.2.1-1
- update to 3.2.1
- use zabbix user and group for Java Gateway
- add SuccessExitStatus=143 for Java Gateway servie file

* Tue Sep 13 2016 Kodai Terashima <kodai.terashima@zabbix.com> - 3.2.0-1
- update to 3.2.0
- add *.conf for Include parameter in agent configuration file

* Mon Sep 12 2016 Kodai Terashima <kodai.terashima@zabbix.com> - 3.2.0rc2-1
- update to 3.2.0rc2

* Fri Sep 09 2016 Kodai Terashima <kodai.terashima@zabbix.com> - 3.2.0rc1-1
- update to 3.2.0rc1

* Thu Sep 01 2016 Kodai Terashima <kodai.terashima@zabbix.com> - 3.2.0beta2-1
- update to 3.2.0beta2

* Fri Aug 26 2016 Kodai Terashima <kodai.terashima@zabbix.com> - 3.2.0beta1-1
- update to 3.2.0beta1

* Fri Aug 12 2016 Kodai Terashima <kodai.terashima@zabbix.com> - 3.2.0alpha1-1
- update to 3.2.0alpha1

* Sun Jul 24 2016 Kodai Terashima <kodai.terashima@zabbix.com> - 3.0.4-1
- update to 3.0.4

* Sun May 22 2016 Kodai Terashima <kodai.terashima@zabbix.com> - 3.0.3-1
- update to 3.0.3
- fix java gateway systemd script to use java options

* Wed Apr 20 2016 Kodai Terashima <kodai.terashima@zabbix.com> - 3.0.2-1
- update to 3.0.2
- remove ZBX-10459.patch

* Sat Apr 02 2016 Kodai Terashima <kodai.terashima@zabbix.com> - 3.0.1-2
- fix proxy packges doesn't have schema.sql.gz
- add server and web packages for RHEL6
- add ZBX-10459.patch

* Sun Feb 28 2016 Kodai Terashima <kodai.terashima@zabbix.com> - 3.0.1-1
- update to 3.0.1
- remove DBSocker parameter

* Sat Feb 20 2016 Kodai Terashima <kodai.terashima@zabbix.com> - 3.0.0-2
- agent, proxy and java-gateway for RHEL 5 and 6

* Mon Feb 15 2016 Kodai Terashima <kodai.terashima@zabbix.com> - 3.0.0-1
- update to 3.0.0

* Thu Feb 11 2016 Kodai Terashima <kodai.terashima@zabbix.com> - 3.0.0rc2
- update to 3.0.0rc2
- add TIMEOUT parameter for java gateway conf

* Thu Feb 04 2016 Kodai Terashima <kodai.terashima@zabbix.com> - 3.0.0rc1
- update to 3.0.0rc1

* Sat Jan 30 2016 Kodai Terashima <kodai.terashima@zabbix.com> - 3.0.0beta2
- update to 3.0.0beta2

* Thu Jan 21 2016 Kodai Terashima <kodai.terashima@zabbix.com> - 3.0.0beta1
- update to 3.0.0beta1

* Thu Jan 14 2016 Kodai Terashima <kodai.terashima@zabbix.com> - 3.0.0alpha6
- update to 3.0.0alpla6
- remove zabbix_agent conf and binary

* Wed Jan 13 2016 Kodai Terashima <kodai.terashima@zabbix.com> - 3.0.0alpha5
- update to 3.0.0alpha5

* Fri Nov 13 2015 Kodai Terashima <kodai.terashima@zabbix.com> - 3.0.0alpha4-1
- update to 3.0.0alpha4

* Thu Oct 29 2015 Kodai Terashima <kodai.terashima@zabbix.com> - 3.0.0alpha3-2
- fix web-pgsql package dependency
- add --with-openssl option

* Mon Oct 19 2015 Kodai Terashima <kodai.terashima@zabbix.com> - 3.0.0alpha3-1
- update to 3.0.0alpha3

* Tue Sep 29 2015 Kodai Terashima <kodai.terashima@zabbix.com> - 3.0.0alpha2-3
- add IfModule for mod_php5 in apache configuration file
- fix missing proxy_mysql alternatives symlink
- chagne snmptrap log filename
- remove include dir from server and proxy conf

* Fri Sep 18 2015 Kodai Terashima <kodai.terashima@zabbix.com> - 3.0.0alpha2-2
- fix create.sql doesn't contain schema.sql & images.sql

* Tue Sep 15 2015 Kodai Terashima <kodai.terashima@zabbix.com> - 3.0.0alpha2-1
- update to 3.0.0alpha2

* Sat Aug 22 2015 Kodai Terashima <kodai.terashima@zabbix.com> - 2.5.0-1
- create spec file from scratch
- update to 2.5.0
