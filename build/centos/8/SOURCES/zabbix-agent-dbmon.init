#!/bin/sh
#
# chkconfig: - 86 14
# description: Zabbix agent dbmon daemon
# processname: zabbix_agentd_dbmon
# config: /etc/zabbix/zabbix_agentd_dbmon.conf
#

### BEGIN INIT INFO
# Provides: zabbix-agent-dbmon
# Required-Start: $local_fs $network
# Required-Stop: $local_fs $network
# Should-Start: zabbix zabbix-proxy
# Should-Stop: zabbix zabbix-proxy
# Default-Start:
# Default-Stop: 0 1 2 3 4 5 6
# Short-Description: Start and stop Zabbix agent dbmon
# Description: Zabbix agent dbmon
### END INIT INFO

# Source function library.
. /etc/rc.d/init.d/functions

if [ -x /usr/sbin/zabbix_agentd_dbmon ]; then
    exec=/usr/sbin/zabbix_agentd_dbmon
else
    exit 5
fi

prog=${exec##*/}
conf=/etc/zabbix/zabbix_agentd_dbmon.conf
pidfile=$(grep -e "^PidFile=.*$" $conf | cut -d= -f2 | tr -d '\r')
timeout=10

if [ -f /etc/sysconfig/zabbix-agent-dbmon ]; then
    . /etc/sysconfig/zabbix-agent-dbmon
fi

if [ -n "$ZABBIX_AGENT_USER" ]; then
    user_conf="--user=$ZABBIX_AGENT_USER"
else
    user_conf=''
fi

lockfile=/var/lock/subsys/zabbix-agent-dbmon

start()
{
    echo -n $"Starting Zabbix agent: "
    daemon $user_conf $exec -c $conf
    rv=$?
    echo
    [ $rv -eq 0 ] && touch $lockfile
    return $rv
}

stop()
{
    echo -n $"Shutting down Zabbix agent: "
    killproc -p $pidfile -d $timeout $prog
    rv=$?
    echo
    [ $rv -eq 0 ] && rm -f $lockfile
    return $rv
}

restart()
{
    stop
    start
}

case "$1" in
    start|stop|restart)
        $1
        ;;
    force-reload)
        restart
        ;;
    status)
        status -p $pidfile $prog 
        ;;
    try-restart|condrestart)
        if status $prog >/dev/null ; then
            restart
        fi
        ;;
    reload)
        action $"Service ${0##*/} does not support the reload action: " /bin/false
        exit 3
        ;;
    *)
	echo $"Usage: $0 {start|stop|status|restart|try-restart|force-reload}"
	exit 2
	;;
esac

